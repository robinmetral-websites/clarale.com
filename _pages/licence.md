---
layout: page
title: Licence
permalink: /licence/
---

This site's source code is licenced under MIT and is available on [Gitlab][gitlab-repo].

Its contents, graphics, and images are CC BY-SA 4.0 unless stated otherwise.

[gitlab-repo]: https://gitlab.com/robinmetral-websites/clarale.com

---
layout: page
title: Coucou, c'est Clara.
permalink: /a-propos/
menu: menu
menutitle: "À propos"
weight: 10
---

Je fais du design graphique
Et aussi du design web avec
: [Robin][robin]

J'aime les cappuccinos et
: les insectes bizarres
: la typographie
: lire jusqu'à tard dans la nuit

Je suis inspirée par des projets engagés comme
: [euforia][euforia]
: [Happy City Lab][happy-city-lab]
: [Future Crunch][future-crunch]

[robin]: https://robinmetral.com
[euforia]: https://euforia.org
[happy-city-lab]: https://happycitylab.com/fr/
[future-crunch]: https://futurecrun.ch

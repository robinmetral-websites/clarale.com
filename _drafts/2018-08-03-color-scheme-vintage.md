---
layout: post
title: "Website Color Schemes: 8 Vintage Palettes"
date: 2018-08-03 +0700 # English translation is a draft
categories: webdesign
excerpt: "I've collected for you the best of the vintage trend, eight websites whose color schemes scream jazz and old paper."
---

We all know how colors are important in web design. They make us *feel* (something we obviously all like) and have the power to give this distinct atmosphere to a website.

Today, I've collected for you the best of the vintage trend. Without further ado, here are eight websites whose color schemes scream jazz and old paper.


# 1. Dark and Bold

![Fieldtrip Color Scheme][image-fieldtrip]

This website by the [FieldTrip][link-fieldtrip] has a color palette that takes a bold, high-contrast approach. It opposes a dark background to warm and striking accents of reds and pinks. This rich and balanced color palette succeeds in being both eye-catching and relaxing.


# 2. Golden Streaks

![Moa Color Scheme][image-moa]

This palette by [Moa][link-moa] also plays with a dark background, which is contrasted with highlights of golds, reds and dark greens. By pairing this elegant golden touch to a black and white image, this site design manages to evoke an antique charm.


# 3. Vibrant and Colorful

![Prepare Color Scheme][image-prepare]

You are looking for a more cheerful atmosphere? This colorful and popping palette is just for you. The bright yellows and reds contrast but do not clash with the other tones. The overall hues are kept warm and consistenteven the green desaturated hue! As for the dominant yellow color, it brings an energetic and fun vibe to the overall design.


# 4. Warm and Earthy

![Frida Color Scheme][image-frida]

When looking at this palette by [café Frida][link-frida], you immediately feel like being at your English grandmother's place for tea. The rich golds, deep greens and earthy browns are balanced by the lighter off-white tones. This creates an elegant and cozy atmosphere.


# 5. Muted and Warm

![Getaway Color Scheme][image-getaway]

Warm muted tones that remind us of old fabric and dusty carpets always achieve this vintage look we long for. This antique-inspired palette by [Getaway][link-getaway] plays with beiges, warm white tones, and shades of reds to create an old and oddly-calming vibe.


# 6. Monochrome

![Mercer Tavern Color Scheme][image-mercer]

Neutral tones often complement more colorful palettes. Here, [Mercer Tavern][link-mercer] took another approach and beautifully showcased a spectrum of whites and greys. Using a grid layout allows the different tones to have their own spaces, and avoids mixing everything up.


# 7. Black and Red

![Cirq Color Scheme][image-cirq]

[Cirq][link-cirq] manages to master the newspaper effect perfectly, without seeming dull. The design warm paper white background is paired with black and blue inks tones. The slightly muted red tints bring character to the overall color palette. The overall effect is simple, yet memorable.


# 8. Splash of Color

![Lowdi Color Scheme][image-lowdi]

This colorful palette by [Lowdi][link-lowdi] is punchy and really makes me want to dance. The warm hues of yellows and salmons contrast with a striking and vibrant teal. The blackest of all blacks also complement the overall design, by balancing all those different colors. 

# How about you?

Seeing those designs, the emotions are quite different from one color scheme to the next, right? Colors have that power and you must choose them carefully while building a website, depending on what emotions you want to produce.

You can easily create and download your own color palette on [colorhunt.co][link-colorhunt] or [coolors.co][link-coolors], so don't miss this important step.

**Bonus**: Here is a [tumblr account][link-tumblr] I love that collects beautiful palettes found in movies by Wes Anderson. <span class="dot"></span>


[image-fieldtrip]: {{ site.baseurl }}{% link assets/images/20180802_color-scheme_vintage_3-fieldtrip.jpg %}
[image-moa]: {{ site.baseurl }}{% link assets/images/20180802_color-scheme_vintage_7-moa.jpg %}
[image-prepare]: {{ site.baseurl }}{% link assets/images/20180802_color-scheme_vintage_8-prepare.jpg %}
[image-frida]: {{ site.baseurl }}{% link assets/images/20180802_color-scheme_vintage_1-frida.jpg %}
[image-getaway]: {{ site.baseurl }}{% link assets/images/20180802_color-scheme_vintage_5-getaway.jpg %}
[image-mercer]: {{ site.baseurl }}{% link assets/images/20180802_color-scheme_vintage_4-mercertavern.jpg %}
[image-cirq]: {{ site.baseurl }}{% link assets/images/20180802_color-scheme_vintage_2-cirq.jpg %}
[image-lowdi]: {{ site.baseurl }}{% link assets/images/20180802_color-scheme_vintage_6-lowdi.jpg %}

[link-fieldtrip]: https://httpster.net/website/the-field-trip/#Mar-07-2012
[link-moa]: https://www.siteinspire.com/websites/2670-moa
[link-frida]: https://httpster.net/website/cafe-frida/
[link-getaway]: https://www.awwwards.com/sites/getaway
[link-mercer]: https://httpster.net/website/mercer-tavern/#Oct-18-2012
[link-cirq]: https://www.siteinspire.com/websites/2940-cirq
[link-lowdi]: https://httpster.net/website/lowdi/
[link-tumblr]: https://wesandersonpalettes.tumblr.com/
[link-colorhunt]: https://colorhunt.co/popular
[link-coolors]: https://coolors.co/browser/best/1

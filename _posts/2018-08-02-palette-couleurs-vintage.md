---
layout: post
title: "Palettes de Couleurs Vintage : 8 Inspirations Web"
date: 2018-08-03 +0700
categories: couleurs, designweb
excerpt: "Le meilleur de la vague vintage en design, huit sites aux palettes de couleurs qui ont du groove."
image: 20180802_color-scheme_vintage_1-frida.jpg
---

On sait tous à quel point composer une palette de couleurs adaptées est important en design web. Les couleurs ont le pouvoir d'engendrer des émotions, de déclencher des envies, de donner une atmosphère distincte à un site. (Oui, c'est beau tout ça.)

Aujourd'hui, j'ai sélectionné pour vous le meilleur de la vague vintage. Voici donc huit sites aux palettes de couleurs qui ont du *groove* (et qui sont jolies aussi).


# 1. Rouge et Noir

![Palette de Couleurs de Fieldtrip][image-fieldtrip]

Cette palette réalisée par [FieldTrip][link-fieldtrip] joue avec les contrastes de luminosité et saturation. Elle oppose un arrière-plan sombre à des accents de rouges et roses. Les tons s'opposent et s'accentuent pour trouver un équilibre à la fois frappant et reposant.


# 2. Accents Dorés

![Palette de Couleurs de Moa][image-moa]

Ce site de [Moa][link-moa] adopte aussi un fond qui tend vers le noir. Par contre, on le parsème plutôt d'éclats dorés, rouges et verts. La superposition de cette palette à une photo lumineuse en noir et blanc parvient presque à nous transporter dans une distillerie des années vingt.


# 3. Vibrant et Coloré

![Palette de Couleurs de Prepare][image-prepare]

Vous avec envie d'une atmosphère plus joyeuse ? Cette palette colorée qui a du peps à revendre est faite pour vous ! Les teintes intenses de jaune et de rouge contrastent avec les autres couleurs, sans pour autant détonner. La tonalité globale reste cohérente et chaude  même le vert légèrement désaturé se marie au reste. Pour ce qui est de la dominante jaune, elle amène de l'énergie et du fun au design.


# 4. Ocre et Chaleureux

![Palette de Couleurs de Frida][image-frida]

Cette palette réalisée par [café Frida][link-frida] me donne immédiatement l'impression d'être chez ma grand-mère anglaise (inexistante) à l'heure du thé. Les tons riches d'or, de vert profond et d'ocre s'équilibrent avec le blanc cassé des fleurs. Résultat: une atmosphère à la fois élégante et chaleureuse.


# 5. Simple et Feutré

![Palette de Couleurs de Getaway][image-getaway]

Les couleurs nous rappelant de vieux meubles poussiéreux et tissus en velours ne manquent jamais leur cible pour ce qui est de créer un look vintage. Cette gamme de couleurs par [Getaway][link-getaway] joue avec les nuances de beige, de blanc et de rouges plus ou moins ternis. Ce mélange de couleurs crée un sentiment apaisant, nostalgique et légèrement mélancolique.


# 6. Monochrome

![Palette de Couleurs de Mercer Tavern][image-mercer]

Les tons neutres sont souvent utilisés pour complémenter les palettes plus colorées. Ici, [Mercer Tavern][link-mercer] a adopté une autre approche en célébrant le gris et ses nuances. Le design utilise une structure en grille permettant d'isoler et de mettre en valeur chaque teinte de gris, en évitant de tout mélanger.


# 7. Papier et Impression

![Palette de Couleurs de Cirq][image-cirq]

[Cirq][link-cirq] réussit parfaitement à maîtriser l'effet journal, sans tomber dans le kitsch ou fade. L'arrière-plan texturé utilise un ton de blanc chaleureux qui est combiné avec des teintes d'encres noires et bleutées. La pointe de rouge amène du caractère à la palette qui reste sobre. Le résultat global est simple, mais marquant.


# 8. Splash de Couleurs

![Palette de Couleurs de Lowdi][image-lowdi]

Cette palette colorée réalisée par [Lowdi][link-lowdi] a du punch et me donne un peu envie de danser (mais faut pas rigoler quand même). Des tons chauds de jaune et saumon contrastent avec un turquoise intense et vibrant. Le plus noir des noirs vient finalement compléter le tout, en harmonisant les différentes couleurs.


# À votre tour !

Vous l'avez remarqué, on ne ressent pas du tout la même chose d'un design à l'autre. Les couleurs ont ce pouvoir et il est fondamental de bien les choisir en design web. Faites votre choix par rapport aux émotions que vous voulez évoquer chez votre audience !

En plus, vous n'avez aucune excuse sachant qu'il existe des plateformes telles que [colorhunt.co][link-colorhunt] ou [coolors.co][link-coolors] qui 
vous permettent de gratuitement et facilement créer vos propres palettes (ou en choisir une dans leur immense base de données).

**Bonus**: Voici un [compte tumblr][link-tumblr] que j'adore, rassemblant de magnifiques palettes de couleurs inspirées des films de Wes Anderson.<span class="dot"></span>


[image-fieldtrip]: {{ site.baseurl }}{% link assets/images/20180802_color-scheme_vintage_3-fieldtrip.jpg %}
[image-moa]: {{ site.baseurl }}{% link assets/images/20180802_color-scheme_vintage_7-moa.jpg %}
[image-prepare]: {{ site.baseurl }}{% link assets/images/20180802_color-scheme_vintage_8-prepare.jpg %}
[image-frida]: {{ site.baseurl }}{% link assets/images/20180802_color-scheme_vintage_1-frida.jpg %}
[image-getaway]: {{ site.baseurl }}{% link assets/images/20180802_color-scheme_vintage_5-getaway.jpg %}
[image-mercer]: {{ site.baseurl }}{% link assets/images/20180802_color-scheme_vintage_4-mercertavern.jpg %}
[image-cirq]: {{ site.baseurl }}{% link assets/images/20180802_color-scheme_vintage_2-cirq.jpg %}
[image-lowdi]: {{ site.baseurl }}{% link assets/images/20180802_color-scheme_vintage_6-lowdi.jpg %}

[link-fieldtrip]: https://httpster.net/website/the-field-trip/#Mar-07-2012
[link-moa]: https://www.siteinspire.com/websites/2670-moa
[link-frida]: https://httpster.net/website/cafe-frida/
[link-getaway]: https://www.awwwards.com/sites/getaway
[link-mercer]: https://httpster.net/website/mercer-tavern/#Oct-18-2012
[link-cirq]: https://www.siteinspire.com/websites/2940-cirq
[link-lowdi]: https://httpster.net/website/lowdi/
[link-tumblr]: https://wesandersonpalettes.tumblr.com/
[link-colorhunt]: https://colorhunt.co/popular
[link-coolors]: https://coolors.co/browser/best/1

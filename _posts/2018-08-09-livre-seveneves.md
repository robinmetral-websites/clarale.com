---
layout: post
title: "Seveneves de Neal Stephenson"
date: 2018-08-09 +0700
categories: livres
excerpt: "Une épopée spatiale post-apocalyptique de la race humaine"
image: "2018-08-09-moon.png"
---

![Illustration Lune par Clara Le][image-lune]

Étant ~~secrètement~~ dans ma période science-fiction, je ne pouvais pas manquer l'incroyable épopée spatiale [*Seveneves*][link-seveneves] de Neal Stephenson.


# Synopsis

> Que se passerait-il si la fin du monde arrivait ?

Suite à la désintégration inexpliquée de la Lune, cette question se retrouve au cœur de l'histoire. On assiste aux efforts désespérés de la race humaine de sauver son Héritage. Sa meilleure option semble être de créer une civilisation spatiale en moins de deux ans.

5000 ans plus tard, la Terre est en ruines mais à nouveau habitable. C'est une race humaine génétiquement très différente qui est prête à revenir s'y installer.


# Quelques notes générales

*Seveneves* appartient avant tout au genre de *hard science-fiction*. Et comme beaucoup de livres du même genre, la dimension technologique est prédominante. 

En gros, si vous avez aimé [*Seul sur Mars*][link-seulsurmars] d'Andy Weir pour ses explications détaillées sur des trucs scientifiques habituellement difficiles à comprendre, ça vous plaira énormément.

Par contre je l'avoue : j'ai lu en diagonal certains des looongs pavés descriptifs très techniques. Mais le bouquin faisant +800 pages, je n'ai jamais eu l'impression de trop manquer.

Pour ce qui est de son format, le livre est divisé en trois parties très bien délimitées qui lui donnne un air de "3 livres en 1". Pfiouuu, les +800 pages intimident tout de suite beaucoup moins.


# Et dans le fond ?

Tout au long du livre, on s'introduit dans la tête de personnages très différents qui parlent en "je". Des astronomes, astronautes, biologistes, journalistes, politiciens, ... 

On s'identifie et on suit leurs parcours très personnels à travers cette épopée. Avec chaque nouveau point de vue, l'image globale se dessine un peu plus.

> Comment vivre lorsqu'il nous reste moins de deux ans ?  
Comment les individus réagissent-ils ?  
Comment la société s'organise-t-elle ?  

Chaque personnage a également son domaine d'expertise. De façon similaire à *Seul sur Mars*, on suit le fil de pensée technique de nombreux d'entre eux. J'adore.

Pour moi, *Seveneves* relate tout d'abord la capacité d'adaptation de l'Homo Sapiens et son instinct de survie. Stephenson aborde les thèmes de la survie individuelle, tout comme la responsabilité de faire perdurer la race humaine.

Une question se pose alors : 
> Y a-t-il une responsabilité à faire perdurer la race humaine ? À quel prix ?


# Qu'est-ce que j'en pense ?

**Ma note** : 4/5

J'ai vraiment beaucoup aimé *Seveneves*. Parfois un peu lent, long, ou trop technique, Stephenson a quand même réussi à me transporter dans sa réalité. Et lorsque je commence à me poser des questions sur l'éthique des décisions prises quand la fin du monde approche, je pense qu'un livre de science-fiction a bien rempli son rôle.



# Infos supplémentaires

**Genres** : hard science-fiction, dystopie
**Année de publication** : 2015
**Pays** : États-Unis


[link-seveneves]: https://www.nealstephenson.com/seveneves.html
[link-seulsurmars]: https://fr.wikipedia.org/wiki/Seul_sur_Mars
[image-lune]: {{ site.baseurl }}{% link assets/images/2018-08-09-moon.png %}
